<?include_once("./includes/config.php")?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?echo $site_title;?></title>
        <link href="css/daziamoore.css" rel="stylesheet" />
        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-3869586-4']);
         _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
    </head>
    <body id="main" style="background-image: url(../images/contact-me.png);background-repeat: no-repeat;background-position: 675px;">
      
            <div id="container_main">
                <p style="position: absolute;top: 85px;">
                  <img src="images/contact.png" />  
                </p>
                <p style="border-top: 1px solid #000;padding-top: 20px; width: 375px;position: absolute;top: 170px;">
                    <b>Dazia Moore</b><br />
                    1000 Seaboard Street, Suite B3<br />
                    Charlotte, NC 28206<br />
                    <br />
                    <b>Booking:</b> 704-343-2620<br />
                    <b>E-Mail:</b> <a href="mailto:bookingdazia@gmail.com">bookingdazia@gmail.com</a>
                    <br />
                    <br />
                    <img src="images/docrocweb.png" alt="Doc Roc Entertainment, LLC" />
                </p>
            </div>
        
    </body>
</html>
