<?include_once("./includes/config.php")?>
<!DOCTYPE html>
<html>
    <head>
        <link href="./css/daziamoore.css" rel="stylesheet" />
        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-3869586-4']);
         _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
    </head>
    <body id="splash">
        <div id="splash-content">
        </div>
    </body>
</html>
