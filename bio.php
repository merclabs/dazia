<!DOCTYPE html>
<? include_once("./includes/config.php") ?>
<html>
    <head>
        <link href="css/daziamoore.css" rel="stylesheet" />
        <script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
        <script>
            $(document).ready(function(){
                $('#biosplash1').fadeIn(3000);
            });
        </script>
        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-3869586-4']);
         _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script> 
    </head>
    <body id="bio">
        <div id="biosplash1"><img src="images/biography-splash1.png" /></div>
        <div id="about"><img src="images/about-dazia-moore.png" /></div>
        <div id="biotext">
            <p>
                Born in Bay Shore, New York, this multicultural beauty knew early
                in life that she wanted to become a successful model/entrepreneur.
                Dazia is determined to make her mark in the world of beauty, fashion
                & entertainment. She Strives to become a role model for other young
                women pursuing their dreams, by being active in her community volunteering
                her time to various organizations to help others. She is quickly
                establishing herself as a force to be reckoned with in the entertainment
                industry and is ready for the many opportunities that will come her way.
            </p>
            <p>
                Dazia has always had an appreciation for the performing arts. At age 10,
                she began playing the cello, performing with her orchestra in venues all
                over New York City. She has also received awards for her literary skills.
                Today, she continues to sharpen her skills as a writer, currently songwriting,
                working on screenplays, as well as her first novel. Growing up, she was always
                taught to have an open mind, this was something she did consistently. While
                in school Dazia was always unique which automatically made her stand out
                so it's not surprising that during her senior year in high school she was
                voted "most likely to become famous", her peers were definitely on to
                something! Dazia's radiance has always been undeniable by many who she
                has come into contact with, she is genuinely a wonderful person, and a joy
                to work with. As a young woman Dazia's aunt told her that "its not
                enough for her to be beautiful, she has to be intelligent as well",
                and those words resonated with her and she has never deviated from
                that path.
            </p>
            <p>
                In all of Dazia's photos, she embodies beauty, class, strength and
                sex appeal frame by frame. She believes in taking care of her body from
                the inside out by drinking water and exercising daily. Dazia is also
                spiritually grounded, and knows that she has to work hard to perfect
                her craft to take her life and career to the next level of progression.
            </p>
            <p>
                Dazia is thankful for the women that have paved the way for her in
                the entertainment and business industry such as Kimora Lee Simmons,
                Jennifer Lopez,and Beyonce Knowles to name a few. These women have
                lived up to the quote that she was taught by her aunt "it's not enough
                to be beautiful,you have to be intelligent as well". Dazia Moore is
                on a mission to build a brand and legacy that she can be proud of.
                Stay tuned...
            </p>
        </div>
    </body>
</html>
