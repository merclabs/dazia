        <?include_once("config.php")?>
        <link href="../css/daziamoore.css" rel="stylesheet" />
        <body>
        <div id="toolbar">
            <div id="container" style="width:1200px;">
            <div align="center">
                <a id="menuitem" href="../main.php" target="center" title="Home">Home</a><img src="../images/toolbar_sep2.jpg" />
                <a id="menuitem" href="../bio.php" target="center" title="About">About</a><img src="../images/toolbar_sep2.jpg" />
                <a id="menuitem" href="../photos.php" target="center" title="Photos">Photos</a><img src="../images/toolbar_sep2.jpg" />
                <a id="menuitem" href="../videos.php" target="center" title="Videos">Videos</a><img src="../images/toolbar_sep2.jpg" />
                <a id="menuitem" href="../media.php" target="center" title="Media">Media</a><img src="../images/toolbar_sep2.jpg" />
                <a id="menuitem" href="../store.php" target="center" title="Store">Store</a><img src="../images/toolbar_sep2.jpg" />
                <a id="menuitem" href="../contact.php" target="center" title="Contact">Contact</a><img src="../images/toolbar_sep2.jpg" />
                <a id="menuitem" href="../redcarpet.php" target="center" title="Red Carpet">Red Carpet</a>
            </div>
            </div>
        </div>
        <div id="footer">
            <div id="container">
                <?echo $site_footer?>
            </div>
        </div>
        </body>
