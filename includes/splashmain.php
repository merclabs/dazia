<?include_once("includes/config.php")?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?echo $site_title;?></title>
        <link href="./css/daziamoore.css" rel="stylesheet" />
        <script type="text/javascript" src="./player/splash/js/swfobject.js"></script>
		
		<script type="text/javascript">

			// JAVASCRIPT VARS
			// cache buster
			var cacheBuster = "?t=" + Date.parse(new Date());		
			
			// stage dimensions
			// if you define '100%' then the swf will have the browser dimensions
			var stageW = "25";//"100%";
			var stageH = "25";//"100%";
			
			
			// ATTRIBUTES
		    var attributes = {};
		    attributes.id = 'FlabellComponent';
		    attributes.name = attributes.id;
		    
			// PARAMS
			var params = {};
			params.bgcolor = "#ffffff";
			

		    /* FLASH VARS */
			var flashvars = {};				
			
			/// if commented / delete these lines, the component will take the stage dimensions defined 
			/// above in "JAVASCRIPT SECTIONS" section or those defined in the settings xml			
			flashvars.componentWidth = stageW;
			flashvars.componentHeight = stageH;
			
			/// path to the content folder(where the xml files, images or video are nested)
			/// if you want to use absolute paths(like "http://domain.com/images/....") then leave it empty("")
			
			flashvars.pathToFiles = "../player/splash/basicsoundplayer/";
			flashvars.xmlPath = "xml/basicsoundplayer.xml";						
			
			/** EMBED THE SWF**/
			swfobject.embedSWF("../player/splash/preview.swf"+cacheBuster, attributes.id, stageW, stageH, "9.0.124", "../player/splash/js/expressInstall.swf", flashvars, params);
			
		</script>

        <script src="http://code.jquery.com/jquery-1.6.4.min.js"></script>
        <script>
            $(document).ready(function(){
              $('#splashimage1').fadeIn(5000).animate({"left": "100"}, "slow");
              $('#splashimage2').fadeIn(5000).animate({"left": "800"}, "slow", function(){
                  $('#splashimage3').fadeIn(3000).animate({"top": "100"}, "slow");
              });
            });
            
        </script> 
    </head>
    <body>
        <div id="container">
            <div id="splashimage3"><img  src="../images/test.jpg" /></div>
            <div id="splashimage2"><img  src="../images/splashimage2.png" /></div>
            <div id="splashimage1"><img  src="../images/splashimage1.png" /></div>
        </div>
        <div id="splashbar">
         <div id="container" style="padding-top: 8px;">
            <div id="FlabellComponent">
                The Player.
            </div>
         </div>
        </div>
    </body>
</html>
