<?include_once("config.php")?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?echo $site_title;?></title>
        <link href="../css/daziamoore.css" rel="stylesheet" />
        <script type="text/javascript" src="../player/main/js/swfobject.js"></script>
		
		<script type="text/javascript">

			// JAVASCRIPT VARS
			// cache buster
			var cacheBuster = "?t=" + Date.parse(new Date());		
			
			// stage dimensions
			// if you define '100%' then the swf will have the browser dimensions
			var stageW = "25";//"100%";
			var stageH = "25";//"100%";
			
			
			// ATTRIBUTES
		    var attributes = {};
		    attributes.id = 'FlabellComponent';
		    attributes.name = attributes.id;
		    
			// PARAMS
			var params = {};
			params.bgcolor = "#000000";
             
			

		    /* FLASH VARS */
			var flashvars = {};	
			
			/// if commented / delete these lines, the component will take the stage dimensions defined 
			/// above in "JAVASCRIPT SECTIONS" section or those defined in the settings xml			
			flashvars.componentWidth = stageW;
			flashvars.componentHeight = stageH;
			
			/// path to the content folder(where the xml files, images or video are nested)
			/// if you want to use absolute paths(like "http://domain.com/images/....") then leave it empty("")
			
			flashvars.pathToFiles = "../player/main/basicsoundplayer/";
			flashvars.xmlPath = "xml/basicsoundplayer.xml";						
			
			/** EMBED THE SWF**/
			swfobject.embedSWF("../player/main/preview.swf"+cacheBuster, attributes.id, stageW, stageH, "9.0.124", "../player/main/js/expressInstall.swf", flashvars, params);
			
		</script>
    </head>
    <body id="main">
        <div id="playerbar">
          <div id="container">
             <div id="FlabellComponent"></div>
             <span id="top-title">OFFICIAL WEBSITE OF MODEL DAZIA MOORE</span>
              <span id="socialmedia">
                  <!-- <a href="http://blog.daziamoore.com" title="Check Out My Blog" target="_blank"><img src="../images/black-blogger.png" border="0" /></a> -->
                  <a href="http://www.facebook.com/dazia.moore" title="Friend me on Facebook" target="_blank"><img src="../images/black-facebook.png" border="0" /></a>
                  <a href="http://www.twitter.com/DaziaMoore" title="Follow me on Twitter" target="_blank"><img src="../images/black-twitter.png" border="0" /></a>
                  <a href="http://www.youtube.com/daziarenee" title="Watch me on YouTube" target="_blank"><img src="../images/black-youtube.png" border="0" /></a>
              </span>
           </div>
        </div>
    </body>
