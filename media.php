<?include_once("./includes/config.php")?>
<!DOCTYPE html>
<html>
    <head>
        <link href="css/daziamoore.css" rel="stylesheet" />
        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-3869586-4']);
         _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
    </head>
    <body id="main">
        <div id="content">
            <div id="container_main">
                <p align="center"><img src="images/media-center.png" alt=""/></p>
                <table id="media">
                    <tr>
                        <td id="mediabox">
                            <a href="mediacenter/kalu.html" title="Dazia at Moscato Monday" target="center">
                                <img src="mediacenter/images/kaluflyer-thumb.jpg" border="0"/>
                            </a>
                        </td>
                        <td id="mediabox">
                            <a href="mediacenter/forbidden.html" title="Forbidden Fridays Cover" target="center">
                                <img src="mediacenter/images/forbidden-fridays-thumb.jpg" border="0"/>
                            </a>
                        </td>
                        <td id="mediabox">
                            <a href="mediacenter/glowmag.html" title="Glow Magazine Cover" target="center">
                                <img src="mediacenter/images/glow-magazine-thumb.jpg" border="0"/>
                            </a>
                        </td>
                    </tr>
                </table>  
            </div>
        </div>
    </body>
</html>
