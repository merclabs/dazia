<?include_once("./includes/config.php")?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?echo $site_title;?></title>
        <link href="css/daziamoore.css" rel="stylesheet" />
        <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-3869586-4']);
         _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
        </script>
    </head>
    <body id="main">
        <div id="content">
            <div id="container_main">
                <div id="video-header"></div>
                <br />
                <table cellspacing="5" style="margin-top: 80px;">
                    <tr>
                        <td>
                            <h2>Dazia for Infinity O2</h2>
                            <div id="mediaplayer1">Problem Loading Video.</div>
                        </td>
                        <!--
                        <td>
                            <h2>Dazia with Ken Harris</h2>
                            <div id="mediaplayer2">Problem Loading Video.</div>
                        </td>
                        -->
                    </tr>
                </table>
            <script type="text/javascript" src="jwplayer/jwplayer.js"></script>
            <script type="text/javascript">
		jwplayer("mediaplayer1").setup({
			flashplayer: "jwplayer/player.swf",
			file: "videos/Dazia_Infinity_Drop.flv",
			image: "videos/infinity-preview.jpg"
		});
            </script>
            <!--
            <script type="text/javascript">
		jwplayer("mediaplayer2").setup({
			flashplayer: "jwplayer/player.swf",
			file: "videos/Dazia_Interview.flv",
			image: "videos/interview-preview.jpg"
		});
            </script>
            -->
            </div>
        </div>
    </body>
</html>
